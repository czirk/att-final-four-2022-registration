const colors = require('tailwindcss/colors')

module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      gray: '#c4c4c4', 
      aluminum: '#848484', 
      silver: '#dddd', 
      white: colors.white, 
      cobalt: '#2058B2', 
      black: 'black'
    }, 
    fill: {
      current: 'currentColor'
    },
    maxWidth: {
      'xl': '30em', 
      '2xl': '40em', 
      '3xl': '64em', 
      '4xl': '82em'
    },
    container: {
      screens: {
        md: "100%",
        lg: "27.875em",
        xl: "63.625em"
      }
    },
    extend: {
      fontFamily: {
        'sans': ['ATT Aleck', 'Arial', 'sans-serif'],
        'condensed': ['ATT Aleck Condensed', 'Arial Condensed', 'sans-serif']
      }, 
      fontSize: {
        'DEFAULT': '0.75rem',
      }, 
      padding: {
        '2': '0.375rem'
      },
      borderWidth: {
        '3': '0.1875rem'
      },
      width: {
        '74': '18.75rem'
      },
    },
    fontFamily: {
      'sans': ['ATT Aleck', 'Arial', 'sans-serif']
    }
  },
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
  content: [
    './public/index.html',
    './src/**/*.js',
    './src/**/*.woff'
  ], 
  plugins: [], 
  darkMode: 'media', // or 'media' or 'class'
}