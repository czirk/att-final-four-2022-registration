const request = require('request-promise');

const KIPI_ENDPOINT = process.env.KIPI_ENDPOINT || 'http://10.2.32.24:5000'
//const KIPI_CLIENT_ID = process.env.KIPI_CLIENT_ID || '6Jw7n6wpxDkws8XgOg42gJz7nkYhIaad'
//const KIPI_CLIENT_SECRET = process.env.KIPI_CLIENT_SECRET || 'ieetS0vnZsT00e6oacCysYoshNQIV60XzWsxEk_sJ9Z-1SdfuXLxtHNDmlAl2y8c';
//const KIPI_AUDIENCE = process.env.KIPI_AUDIENCE || 'https://kipidev.com/api/v1'
//const KIPI_TOKEN_HOST = 'https://bluewater.auth0.com';
const KIPI_PROJECT_ID = process.env.KIPI_PROJECT_ID || '16W3Zhs1e48JBOxvbfjIWu7lnIx';

// refresh 5 minutes before
const REFRESH_BUFFER_MILLISECONDS = 1000 * 60 * 5;

let token;
let expiresAt;

const tokenHasExpired = () => {
  if (!token || expiresAt <= Date.now()) {
    return true;
  }
}
/*
const refreshToken = async () => {
  console.log('refreshing kipi token');
  try {
    const resp = await request(`${KIPI_TOKEN_HOST}/oauth/token`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: {
        'client_id': KIPI_CLIENT_ID,
        'client_secret': KIPI_CLIENT_SECRET,
        'audience': KIPI_AUDIENCE,
        'grant_type': 'client_credentials'
      },
      json: true
    })

    if (!resp.access_token || !resp.expires_in) {
      console.log('invalid access token response', resp);
      return Promise.reject(new Error('invalid response from token request'))
    }

    token = resp.access_token;
    expiresAt = new Date((resp.expires_in * 1000) + (new Date()).getTime());

    console.log('token expires at: ' + expiresAt);

    const refreshIn = ((resp.expires_in * 1000) - REFRESH_BUFFER_MILLISECONDS);

    const refreshAt = new Date((new Date()).getTime() + refreshIn);
    console.log('scheduling refresh for: ' + refreshAt);

    setTimeout(() => refreshToken(), refreshIn)
    return resp;
  } catch (err) {
    console.log('error refreshing token: ' + err.message)
    return Promise.reject(err);
  }

}
*/
const delay = (millis) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), millis)
  })
}

const createLead = async (data) => {

  //let resp;

  try {
    let resp = await Promise(`${KIPI_ENDPOINT}/api/register/`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: {
        project_id: KIPI_PROJECT_ID,
        ...data
      },
      json: true
    })
  } catch (err) {
    console.error('api error: ', err.message);
    return Promise.reject(err);
  }


  return Promise.resolve();
}



/*
const createLead = async (data) => {

  let resp;

  if (tokenHasExpired()) {
    try {
      await refreshToken();
    } catch (err) {
      return Promise.reject(err);
    }
  }

  //await delay(3000);

  try {
    resp = await request(`${KIPI_ENDPOINT}/v1/contacts/`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        'authorization': `Bearer ${token}`
      },
      body: {
        project_id: KIPI_PROJECT_ID,
        ...data
      },
      json: true
    })
  } catch (err) {
    console.error('api error: ', err.message);
    return Promise.reject(err);
  }


  return Promise.resolve();
}
*/


module.exports = {
//  refreshToken,
  createLead
}

