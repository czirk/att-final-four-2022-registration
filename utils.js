module.exports.splitNames = (name) => {
  const namesplit = name.split(' ');

  const lastName = namesplit.pop();

  const firstName = (namesplit.length) === 0 ?
    '' :
    namesplit.join(' ')

  return {
    first_name: firstName,
    last_name: lastName
  }
}
