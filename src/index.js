import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker'
///
import Home from './pages/Home'
import Register from './pages/Register'
import Header from './partials/Header'

const index = document.querySelector('[data-index]')

const Index = () => (
  <BrowserRouter>
    <Header/>
    <Routes>
      <Route index element={<Home/>}/>
      <Route path="/register" exact element={<Register/>}/>
      <Route path="/register:age" element={<Register age={13}/>}/>
    </Routes> 
  </BrowserRouter>
)

if(index) ReactDOM.render(<Index/>, index)

registerServiceWorker()
