import React from 'react'
import { NavLink } from 'react-router-dom'

const Header = () => {
  return (
    <header className="fixed top-0 left-8 right-8 border border-white border-l-0 border-r-0 border-t-0 flex justify-between items-center">
      <NavLink to="/" className="flex flex-col w-88 h-20 py-4 ml-0 mr-auto text-white">
        <svg className="h-full w-full pr-4" style={{maxWidth:'80%'}}>
          <use xlinkHref="#logo__att--fan_zone"/>
        </svg>
      </NavLink>
      <NavLink to="/" className="flex flex-col w-88 h-20 py-4 mr-0 ml-auto text-white">
        <img className="h-full w-full pl-4" alt="" src="./images/logos__att__final_four.svg"/>
      </NavLink>
    </header>
  )
}

export default Header