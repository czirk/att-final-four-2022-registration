import React from 'react'

const Heading = () => {
  return (
    <figure className="order-1 flex flex-col items-center w-full p-4">
      <svg className="w-full h-auto my-8 text-white" title="AT&amp;T RHYTHM OF THE GAME">
        <use xlinkHref="#logo__att--rhythm_of_the_game"/>
      </svg>
    </figure>
  )
}

export default Heading