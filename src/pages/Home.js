import React, { useEffect, useRef } from 'react'
import { NavLink } from 'react-router-dom'
import {attachPaths} from '../global'
import Heading from '../partials/Heading'

const Home = () => {
  let aStyle = "flex flex-col items-stretch justify-center content-center w-1/2 h-full py-4 px-16 bg-cobalt rounded-full border border-3 border-white text-white shadow-sm text-center text-2xl font-black uppercase"

  attachPaths()

  return (
    <section className="flex flex-col items-center w-2/3 h-full pt-24 mx-auto">
      <h1 className="order-2 mt-16 mb-12 text-3xl text-white font-black">Are you at least 13 years of age?</h1>
      <nav className="order-3 flex items-stretch content-center justify-stretch space-x-8">
        <NavLink
          to="/register" className={aStyle}>I am 12<br/> or&nbsp;under</NavLink>
        <NavLink
          to="/register?age=13" className={aStyle}>I am 13<br/> or&nbsp;over</NavLink>
      </nav>
      <Heading/>
    </section>
  )
}

export default Home
