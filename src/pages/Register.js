import React, { useState, useRef } from 'react'
import Heading from '../partials/Heading'
import { attachPaths } from '../global'

const Register = () => {

  const [loading, setLoading] = useState(false)
  let age = useRef()
  
  const cStyle = "p-4 appearance-none checked:bg-cobalt bg-white mr-8 mt-1 text-3xl"
  const iStyle = "w-full bg-transparent px-8 py-4 text-xl font-black text-white border-white border-2 rounded-full placeholder-white focus:placeholder-black focus:outline-none focus:border-cyan focus:bg-white uppercase"

  const registerPlayer = (event) => {
    event.preventDefault()
    setLoading(true)

    let name = document.getElementById('display_name')
    let email = document.getElementById('email')
    let phone = document.getElementById('phone')
    let privacy = document.getElementById('privacy')
    let contact = document.getElementById('contact')
    
    if(phone) phone.value = phone.value.replace(/^(\+)|\D/g, '$1')
    
    let lead = {
      display_name: name.value,
      email: email ? email.value : '', 
      phone: phone.value,
      privacy: privacy.value === 'on' ? true : false,
      contact: contact.value === 'on' ? true : false
    }

    fetch('/leads', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(lead)
    })
    .then(res => {
      const json = res.json()
      if (!res.ok) {
        console.error('unable to create lead: ' + (json.message || ''))
      }
      setLoading(false)
    })
    .then(body => {
      setLoading(false)
    })
    .catch((err) => {
      alert(err.message);
      console.error(err);
      setLoading(false)
    })
  }

  age = attachPaths()

  return (
    <form className="flex flex-col items-center w-3/5 h-full pt-20 mx-auto" onSubmit={registerPlayer}>
      <fieldset className="space-y-4 order-2 mb-8 w-3/5">
        <input
          id="display_name" className={iStyle}
          placeholder="Display Name" defaultValue='' 
          required minLength="1" maxLength="50" type="text"/>
        <input
          id="phone" className={iStyle}
          defaultValue='' placeholder="Phone Number"
          required type="tel"/>
        { age === 13 &&
        <input rel="underage"
          id="email" className={iStyle}
          defaultValue='' placeholder="Email Address"
          minLength="8"
          maxLength="50"
          required type="email"/>
        }
      </fieldset>
      <fieldset className={`space-y-4 order-3 mb-8 w-3/5 text-xs text-white${ age !== 13 ? ' hidden' : ''}`}>
        <div className="flex no-wrap justify-start items-start">
          <input id="privacy" type="checkbox" className={cStyle} defaultChecked={false}/>
          <label htmlFor="privacy" className="font-condensed">By proceeding with this registration I agree and consent that AT&amp;T and its affiliated companies as well as third parties acting on AT&amp;T's behalf may process personal data from or about me as outlined in the AT&amp;T <a href="https://about.att.com/privacy/full_privacy_policy.html" target="_blank"> Privacy Policy</a> necessary to register me and facilitate my participation at the event.</label>
        </div>
        <div className="flex no-wrap justify-start items-start">
          <input id="contact" type="checkbox" className={cStyle} defaultChecked={false}/>
          <label htmlFor="contact" className="font-condensed">By submitting your email address you agree to receive future emails from AT&amp;T and its family of companies. We'll email you offers and promotions about AT&amp;T and other AT&amp;T products and services.</label>
        </div>
      </fieldset>
      <nav className="space-y-8 order-3 w-3/5 mx-auto mb-4">
        <button className={`${loading ? 'disabled ' : ''}flex items-stretch justify-center content-center mx-auto px-16 py-4 bg-cobalt rounded-full border border-white text-white shadow-sm text-center text-xl font-bold uppercase`}>
          <span className="">Register</span>
        </button>
      </nav>
      <Heading/>
    </form>
  )
}

export default Register