import React from 'react'
import { returnConfirmation } from '../global'

const Confirmation = () => {
  return (
    <div className="w-2/3 bg-white h-full p-4 overflow-hidden flex flex-col">
      <header className="relative mb-8">
        <h3 className="text-4xl mb-8">Confirmation</h3>
        <button className="absolute right-0 top-0 bg-white hover:text-cyan text-black text-4xl w-12 h-12 text-center flex flex-col items-center content-center"
         onClick={returnConfirmation}>
          <svg className="w-full h-full">
            <use xlinkHref="#icon__math--multiply"/>
          </svg>
        </button>
      </header>
      <div className="overflow-y-scroll">
        <p data-display_name className="text-4xl"></p>
        <p className="text-6xl">Thanks, you're signed up!</p>
      </div>
    </div>
  )
}

export default Confirmation