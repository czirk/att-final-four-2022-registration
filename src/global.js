export const getPaths = () => {
  return window.location.pathname.split('/').map(path => path)
}

export const attachPaths = () => {
  const body = document.querySelector('body')
  const paths = getPaths()
  if(paths[1] !== '') body.classList.add(paths[1])
  const locus = window.location.href
  return parseFloat(locus.substring(locus.search('age=') + 4))
}