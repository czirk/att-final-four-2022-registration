const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const sslRedirect = require('heroku-ssl-redirect')
const kipi = require('./kipi')
const { splitNames } = require('./utils')

// express app instance
const app = express()

// environment variables
const PORT = process.env.PORT || '4000'

// https redirect
app.use(sslRedirect());

app.use(express.static('build'))

app.use(bodyParser.json());

app.post('/leads', async (req, res) => {

  try {

    console.log('request received', req.body);

    const {
      first_name,
      last_name,
      display_name,
      email,
      permission
    } = req.body;


    await kipi.createLead({
      first_name,
      last_name,
      display_name,
      email: email.trim(),

      user_data: {
        permission
      }
    })

    return res.status(200).json({
      message: 'ok'
    })

  } catch (err) {
    console.error('lead submission error:', err.message);
    return res.status(500).json({ message: 'An unknown error occurred' })
  }
})

app.get('/robots.txt', (req, res) => {
  return res.sendFile(path.resolve(___dirname, '../build/robots.txt'));
})

app.get('*', (req, res) => {
  return res.sendFile(path.resolve(___dirname, '../build/index.html'));
})

async function startup() {
  try {

    console.log('starting app in NODE_ENV: ' + process.env.NODE_ENV);

 //   await kipi.refreshToken();

    app.listen(PORT, () => console.log(`walmart leadgen app listening on port ${PORT}`))

  } catch (err) {
    console.error(err);
  }
}

startup();
